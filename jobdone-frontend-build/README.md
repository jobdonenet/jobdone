# jobdone-frontend-build

In order to make backend work, modify your `config.py` file and set the following properties:
```
CUSTOM_TEMPLATE_FOLDER = '/vagrant_frontend/jobdone-frontend-build/templates'
FRONTEND_BUILD_LOCATION = '/vagrant_frontend/jobdone-frontend-build/assets'
```
