"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const child_process_1 = require("child_process");
const dotenv = require("dotenv");
const _1 = require(".");
dotenv.config({ silent: true });
main()
    .then(() => {
    console.log('Exiting without errors');
    process.exit(0);
})
    .catch(err => {
    console.error(err);
    process.exit(1);
});
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        yield _1.default.connect(process.env.DB_CONNECTION_STRING);
        let config = _1.default.getConnectionConfig();
        yield runRestoreAsync('DO 0;', config);
        let result = yield _1.default.query('SELECT COUNT(*) as count FROM information_schema.tables WHERE table_schema = ? AND table_name = ?', [
            config.database,
            'config'
        ]);
        let isDatabaseEmpty = !result[0].count;
        let currentVersion = 0;
        if (!isDatabaseEmpty) {
            let result = yield _1.default.query('SELECT `value` FROM config WHERE `key` = ?', ['database.version']);
            if (result.length) {
                currentVersion = parseInt(result[0].value);
            }
        }
        console.log('Current database version is: %d', currentVersion);
        let migrationVersions = readMigrationVersionsSync();
        if (!migrationVersions.length) {
            console.log('No migrations available');
            return;
        }
        migrationVersions.sort();
        let latestVersion = migrationVersions[migrationVersions.length - 1];
        console.log('Latest version available: %d', latestVersion);
        if (latestVersion === currentVersion) {
            console.log('Database is up to date');
            return;
        }
        while (currentVersion < latestVersion) {
            currentVersion++;
            if (migrationVersions.indexOf(currentVersion) === -1) {
                console.log('Skipping version %d as it is not found', currentVersion);
                continue;
            }
            console.log('Applying version %d...', currentVersion);
            yield applyMigration(currentVersion, config);
            console.log('Version %d has been successfully applied', currentVersion);
            yield _1.default.query('REPLACE INTO config (`key`, `value`) VALUES (?, ?)', [
                'database.version', currentVersion
            ]);
        }
    });
}
function readMigrationVersionsSync() {
    let items = fs.readdirSync(path.resolve(__dirname, 'versions')), migrationVersions = [];
    for (let item of items) {
        let num = +item, dirPath = path.resolve(__dirname, 'versions', item);
        if (!isNaN(num) && fs.statSync(dirPath).isDirectory()) {
            migrationVersions.push(num);
        }
    }
    return migrationVersions;
}
function applyMigration(version, config) {
    return __awaiter(this, void 0, void 0, function* () {
        let items = fs.readdirSync(path.resolve(__dirname, 'versions', version.toString())), migrationFiles = [];
        for (let item of items) {
            if (/^\d+_?.*\.(js|sql)$/i.test(item)) {
                migrationFiles.push(item);
            }
        }
        migrationFiles.sort((a, b) => {
            return parseInt(a) - parseInt(b);
        });
        for (let file of migrationFiles) {
            let filePath = path.resolve(__dirname, 'versions', version.toString(), file);
            if (file.endsWith('sql')) {
                console.log('Running SQL %s...', file);
                yield runRestoreAsync(fs.readFileSync(filePath).toString(), config);
            }
        }
    });
}
function runRestoreAsync(sql, config) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            let args = [
                '-u',
                config.user,
                '--host',
                config.host,
                '-p',
                config.database
            ];
            let proc = child_process_1.spawn('mysql', args, { stdio: ['pipe', 'inherit', 'inherit'] })
                .on('error', err => {
                return reject(err);
            })
                .on('exit', (code, signal) => {
                if (signal) {
                    return reject(new Error('MySQL import terminated with signal ' + signal));
                }
                if (code !== 0) {
                    return reject(new Error('MySQL import terminated with code ' + code));
                }
                resolve();
            });
            proc.stdin.on('error', () => { });
            proc.stdin.write(sql);
            proc.stdin.end();
        });
    });
}
