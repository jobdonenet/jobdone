ALTER TABLE `rooms` ADD COLUMN `last_message_id` INT(11) UNSIGNED NULL DEFAULT NULL;

UPDATE `rooms` r SET `last_message_id` = (SELECT MAX(`id`) FROM `messages` WHERE `room_id` = r.id);