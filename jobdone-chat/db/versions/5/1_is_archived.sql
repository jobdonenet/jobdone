ALTER TABLE `participants` ADD COLUMN `is_archived` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';

UPDATE `participants` r SET `is_archived` = '0';
