CREATE TABLE `rooms` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `type` VARCHAR(20) NOT NULL,
    `entity_id` INT(11) UNSIGNED NULL DEFAULT NULL,
    `is_empty` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
    `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `last_action_date` TIMESTAMP NULL DEFAULT NULL,
    `meta_json` TEXT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `type_entity_id` (`type`, `entity_id`),
    INDEX `type` (`type`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


CREATE TABLE `messages` (
    `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
    `room_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
    `user_id` INT(11) UNSIGNED DEFAULT NULL,
    `type` VARCHAR(30) NOT NULL,
    `message` TEXT NULL,
    `meta_json` TEXT NULL,
    `create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    INDEX `FK_messages_rooms` (`room_id`),
    CONSTRAINT `FK_messages_rooms` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


CREATE TABLE `participants` (
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `room_id` INT(11) UNSIGNED NOT NULL,
    `user_id` INT(11) UNSIGNED NOT NULL,
    `last_read_message_id` BIGINT(20) UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `room_id_user_id` (`room_id`, `user_id`),
    CONSTRAINT `FK_participants_rooms` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT `FK_participants_messages` FOREIGN KEY (`last_read_message_id`) REFERENCES `messages` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
