ALTER TABLE `participants` ADD COLUMN `last_notified_message_id` BIGINT(20) UNSIGNED DEFAULT NULL;

UPDATE `participants` r SET `last_notified_message_id` = r.`last_read_message_id`;