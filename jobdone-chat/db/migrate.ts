import * as fs from 'fs';
import * as path from 'path';
import { spawn } from 'child_process';
import * as dotenv from 'dotenv';
import db from '.';


// Initializing .env file
dotenv.config({ silent: true });


main()
    .then(() => {
        console.log('Exiting without errors');
        process.exit(0);
    })
    .catch(err => {
        console.error(err);
        process.exit(1);
    });


async function main() {
    await db.connect(process.env.DB_CONNECTION_STRING);

    let config = db.getConnectionConfig();

    // Run a simple statement to ensure mysql command is available and settings are correct
    await runRestoreAsync('DO 0;', config);

    let result = await db.query('SELECT COUNT(*) as count FROM information_schema.tables WHERE table_schema = ? AND table_name = ?', [
        config.database,
        'config'
    ]);

    let isDatabaseEmpty = !result[0].count;

    let currentVersion = 0;
    if (!isDatabaseEmpty) {
        // Retrieve current version
        let result = await db.query('SELECT `value` FROM config WHERE `key` = ?', ['database.version']);

        if (result.length) {
            currentVersion = parseInt(result[0].value);
        }
    }

    console.log('Current database version is: %d', currentVersion);

    let migrationVersions = readMigrationVersionsSync();

    if (!migrationVersions.length) {
        console.log('No migrations available');
        return;
    }

    migrationVersions.sort();

    let latestVersion = migrationVersions[migrationVersions.length - 1];

    console.log('Latest version available: %d', latestVersion);

    if (latestVersion === currentVersion) {
        console.log('Database is up to date');
        return;
    }

    while (currentVersion < latestVersion) {
        currentVersion++;

        if (migrationVersions.indexOf(currentVersion) === -1) {
            console.log('Skipping version %d as it is not found', currentVersion);
            continue;
        }

        console.log('Applying version %d...', currentVersion);

        await applyMigration(currentVersion, config);

        console.log('Version %d has been successfully applied', currentVersion);

        // Update database version in the config
        await db.query('REPLACE INTO config (`key`, `value`) VALUES (?, ?)', [
            'database.version', currentVersion
        ]);
    }
}


function readMigrationVersionsSync(): number[] {
    let items = fs.readdirSync(path.resolve(__dirname, 'versions')),
        migrationVersions = [];

    for (let item of items) {
        let num = +item,
            dirPath = path.resolve(__dirname, 'versions', item);

        if (!isNaN(num) && fs.statSync(dirPath).isDirectory()) {
            migrationVersions.push(num);
        }
    }

    return migrationVersions;
}


async function applyMigration(version: number, config: any) {
    let items = fs.readdirSync(path.resolve(__dirname, 'versions', version.toString())),
        migrationFiles = [];

    for (let item of items) {
        if (/^\d+_?.*\.(js|sql)$/i.test(item)) {
            migrationFiles.push(item);
        }
    }

    migrationFiles.sort((a, b) => {
        // parseInt will basically take the first number out of the name
        return parseInt(a) - parseInt(b);
    });

    for (let file of migrationFiles) {
        let filePath = path.resolve(__dirname, 'versions', version.toString(), file);

        if (file.endsWith('sql')) {
            console.log('Running SQL %s...', file);
            await runRestoreAsync(fs.readFileSync(filePath).toString(), config);
        }
    }
}


async function runRestoreAsync(sql: string, config: any) {
    return new Promise((resolve, reject) => {
        let args = [
            '-u',
            config.user,
            '--host',
            config.host,
            '-p',
            config.database
        ];

        let proc = spawn('mysql', args, { stdio: ['pipe', 'inherit', 'inherit'] })
            .on('error', err => {
                return reject(err);
            })
            .on('exit', (code, signal) => {
                if (signal) {
                    return reject(new Error('MySQL import terminated with signal ' + signal));
                }

                if (code !== 0) {
                    return reject(new Error('MySQL import terminated with code ' + code));
                }

                resolve();
            });

        proc.stdin.on('error', () => {});
        proc.stdin.write(sql);
        proc.stdin.end();
    });
}
