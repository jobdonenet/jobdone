"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql = require("mysql2");
class Database {
    connect(connectionString) {
        return __awaiter(this, void 0, void 0, function* () {
            this.pool = yield mysql.createPool(connectionString);
        });
    }
    getConnectionConfig() {
        if (!this.pool) {
            return {};
        }
        return this.pool.config.connectionConfig;
    }
    query(sql, replacements = null) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                this.pool.getConnection((err, connection) => {
                    if (err) {
                        return reject(err);
                    }
                    connection.query(sql, replacements, (err, result, fields) => {
                        connection.release();
                        if (err) {
                            return reject(err);
                        }
                        resolve(result);
                    });
                });
            });
        });
    }
    queryOne(sql, replacements = null) {
        return __awaiter(this, void 0, void 0, function* () {
            let results = yield this.query(sql, replacements);
            return results.length ? results[0] : null;
        });
    }
    insertOne(sql, replacements = null) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                this.pool.getConnection((err, connection) => {
                    if (err) {
                        return reject(err);
                    }
                    connection.query(sql, replacements, (err, result, fields) => {
                        connection.release();
                        if (err) {
                            return reject(err);
                        }
                        resolve(result.insertId);
                    });
                });
            });
        });
    }
}
const database = new Database();
exports.default = database;
