import * as mysql from 'mysql2';


class Database {
    private pool: any;

    async connect(connectionString: string) {
        this.pool = await mysql.createPool(connectionString);

        // this.pool.on('connection', function(connection) {
        //     connection.on('enqueue', function(sequence) {
        //         // if (sequence instanceof mysql.Sequence.Query) {
        //         if (sequence.sql) {
        //             console.log(sequence.sql);
        //         }
        //     });
        // });
    }

    getConnectionConfig() {
        if (!this.pool) {
            return {};
        }

        return this.pool.config.connectionConfig;
    }
    
    async query(sql, replacements = null): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            this.pool.getConnection((err, connection) => {
                if (err) {
                    return reject(err);
                }

                connection.query(sql, replacements, (err, result, fields) => {
                    connection.release();

                    if (err) {
                        return reject(err);
                    }

                    resolve(result);
                });
            });
        });
    }

    async queryOne(sql, replacements = null): Promise<any> {
        let results = await this.query(sql, replacements);
        return results.length ? results[0] : null;
    }

    async insertOne(sql, replacements = null): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            this.pool.getConnection((err, connection) => {
                if (err) {
                    return reject(err);
                }

                connection.query(sql, replacements, (err, result, fields) => {
                    connection.release();

                    if (err) {
                        return reject(err);
                    }

                    resolve(result.insertId);
                });
            });
        });
    }
}


const database = new Database();
export default database;
