import { randomBytes } from 'crypto';
import { info, error, debug } from 'winston';

import * as utils from './utils';


class Messaging {
    private connections: any = {};
    private userConnections: any = {};
    private socketServer: any;

    initialize(socketServer: any) {
        this.socketServer = socketServer;

        this.socketServer.on('connection', conn => {
            // Patch connection with metadata we will use later
            conn.meta = {
                authToken: this.generateAuthToken()
            };

            this.connections[conn.id] = conn;

            // Send auth token to the client, which will be used to authenticate connection through backend
            conn.write(JSON.stringify({
                t: 'auth.token',
                b: {
                    cid: conn.id,
                    token: conn.meta.authToken
                }
            }));

            conn.on('data', data => {
                let msg = JSON.parse(data);

                if (conn.meta.userID) {
                    this.handleAuthenticatedSocketMessage(conn, msg);
                }
            });

            conn.on('close', () => {
                delete this.connections[conn.id];

                if (conn.meta && conn.meta.userID && this.userConnections[conn.meta.userID]) {
                    delete this.userConnections[conn.meta.userID][conn.id];
                }
            });
        });
    }

    public sendNotification(userID: number, type: string, date: number, meta: any) {
        console.log('Send notification to', userID, type, date);

        let connections = this.userConnections[userID];

        let body = {
            meta,
            type,
            date
        };

        for (let cid in connections) {
            connections[cid].write(this.composeMessage('notification', body));
        }
    }

    public sendMessage(userID: number, roomStringID: string, type: string, date: number, meta?: any, text?: any, sender?: number) {
        let connections = this.userConnections[userID];

        let body = {
            meta,
            type,
            date,
            text,
            user: sender
        };

        for (let cid in connections) {
            connections[cid].write(this.composeMessage(`message.${roomStringID}`, body));
        }
    }

    public sendMessageUpdate(messageID: number, userID: number, roomStringID: string, text?: string, meta?: any) {
        let connections = this.userConnections[userID];

        let body = {
            meta,
            text,
            id: messageID
        };

        for (let cid in connections) {
            connections[cid].write(this.composeMessage(`message.update.${roomStringID}`, body));
        }
    }

    private handleAuthenticatedSocketMessage(connection: any, message: any) {
        console.log('got message', message);

        let type = message.t,
            typeSplitted = type.split('.'),
            body = message.b,
            uid = message.u;

        if (typeSplitted[0] === 'history' && typeSplitted[1]) {
            this.handleGetHistory(connection, typeSplitted[1], body, uid);
        }

        if (typeSplitted[0] === 'message' && typeSplitted[1]) {
            this.handleSendMessage(connection, typeSplitted[1], body);
        }

        if (typeSplitted[0] === 'rooms' && typeSplitted[1]) {
            this.handleGetRooms(connection, typeSplitted[1], body, uid);
        }

        if (typeSplitted[0] === 'room' && typeSplitted[1]) {
            this.handleGetRoom(connection, typeSplitted[1], body, uid);
        }

        if (typeSplitted[0] === 'count') {
            this.handleGetCount(connection, body);
        }

        if (typeSplitted[0] === 'archive') {
            this.handleArchiveRoom(connection, body);
        }
    }

    private handleGetHistory(connection: any, roomStringID: string, body: any, uid: any) {
        let fnSendError = (err, statusCode) => {
            error(err);
            connection.write(this.composeMessage(`history.${roomStringID}`, { error: { status: statusCode } }, uid));
        };

        utils.checkRoomParticipant(roomStringID, connection.meta.userID).then(allowed => {
            if (!allowed) {
                fnSendError(new Error(`User ${connection.meta.userID} is not allowed to read room ${roomStringID}`), 403);
                return;
            }

            utils.getMessages(roomStringID, body, connection.meta.userID).then(messages => {
                let fnSendMessages = (markAsRead = false) => {
                    connection.write(this.composeMessage(`history.${roomStringID}`, messages, uid));

                    if (markAsRead) {
                        connection.write(this.composeMessage('read', { room: roomStringID }, uid));
                    }
                };

                if (body.markAsRead) {
                    utils.markRoomAsRead(roomStringID, connection.meta.userID).then(fnSendMessages.bind(null, true)).catch(fnSendMessages);
                } else {
                    fnSendMessages();
                }
            }).catch(err => fnSendError(err, 500));
        }).catch(err => fnSendError(err, 500));
    }

    private handleGetRooms(connection: any, type: string, body: any, uid: any) {
        let fnSendError = (err, statusCode) => {
            error(err);
            connection.write(this.composeMessage(`rooms.${type}`, { error: { status: statusCode } }, uid));
        };

        utils.getRoomsWithCount(type, connection.meta.userID, body).then(data => {
            connection.write(this.composeMessage(`rooms.${type}`, data, uid));
        }).catch(err => fnSendError(err, 500));
    }

    private handleGetRoom(connection: any, roomStringID: string, body: any, uid: any) {
        let fnSendError = (err, statusCode) => {
            error(err);
            connection.write(this.composeMessage(`room.${roomStringID}`, { error: { status: statusCode } }, uid));
        };

        utils.checkRoomParticipant(roomStringID, connection.meta.userID).then(allowed => {
            if (!allowed) {
                fnSendError(null, 403);
                return;
            }

            let parts = roomStringID.split(':');

            utils.getRoomComplete(parts[0], +parts[1]).then(room => {
                connection.write(this.composeMessage(`room.${roomStringID}`, room, uid));
            }).catch(err => fnSendError(err, 500));
        }).catch(err => fnSendError(err, 500));
    }

    private handleGetCount(connection: any, body: any) {
        let fnSendError = (err, statusCode) => {
            error(err);
            connection.write(this.composeMessage(`count`, { error: { status: statusCode } }));
        };

        utils.getRoomsCount(body.rooms, connection.meta.userID).then(data => {
            connection.write(this.composeMessage(`count`, data));
        }).catch(err => fnSendError(err, 500));
    }

    private handleArchiveRoom(connection: any, body: any) {
        let fnSendError = (err, statusCode) => {
            error(err);
            connection.write(this.composeMessage(`archive`, { error: { status: statusCode } }));
        };

        utils.archiveRooms(body.rooms, connection.meta.userID).then(rooms => {
            connection.write(this.composeMessage(`archive`, { rooms: rooms }));
        }).catch(err => fnSendError(err, 500));
    }

    private handleSendMessage(connection: any, roomStringID: string, body: any) {
        let fnSendError = (statusCode) => {
            connection.write(this.composeMessage(`message.${roomStringID}`, { error: { status: statusCode } }));
        };

        utils.checkRoomParticipant(roomStringID, connection.meta.userID).then(allowed => {
            if (!allowed) {
                fnSendError(403);
                return;
            }

            utils.createMessage(roomStringID, connection.meta.userID, { text: body.text, meta: body.meta, type: 'message' }).then().catch(err => fnSendError(500));
        }).catch(err => fnSendError(500));
    }

    private composeMessage(type: string, body: any, uid?: any) {
        let msg: any = { t: type, b: body };
        if (uid) {
            msg.u = uid;
        }

        return JSON.stringify(msg);
    }

    private generateAuthToken(): string {
        return randomBytes(32).toString('hex');
    }

    public authenticateConnection(cid: any, token: string, data: any) {
        if (!(cid in this.connections)) {
            return;
        }

        let connection = this.connections[cid],
            meta = connection.meta;

        if (!meta || !meta.authToken || meta.authToken !== token) {
            // Wrong token provided, close connection
            connection.close();
            return;
        }

        // Authentication was successful

        meta = Object.assign({}, meta, {
            userID: data.userID
        });

        connection.meta = meta;

        if (!this.userConnections[connection.meta.userID]) {
            this.userConnections[connection.meta.userID] = {};
        }

        this.userConnections[connection.meta.userID][cid] = connection;

        connection.write(JSON.stringify({
            t: 'auth.success'
        }));
    }

}


const messaging = new Messaging();
export default messaging;