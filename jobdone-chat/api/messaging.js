"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = require("crypto");
const winston_1 = require("winston");
const utils = require("./utils");
class Messaging {
    constructor() {
        this.connections = {};
        this.userConnections = {};
    }
    initialize(socketServer) {
        this.socketServer = socketServer;
        this.socketServer.on('connection', conn => {
            conn.meta = {
                authToken: this.generateAuthToken()
            };
            this.connections[conn.id] = conn;
            conn.write(JSON.stringify({
                t: 'auth.token',
                b: {
                    cid: conn.id,
                    token: conn.meta.authToken
                }
            }));
            conn.on('data', data => {
                let msg = JSON.parse(data);
                if (conn.meta.userID) {
                    this.handleAuthenticatedSocketMessage(conn, msg);
                }
            });
            conn.on('close', () => {
                delete this.connections[conn.id];
                if (conn.meta && conn.meta.userID && this.userConnections[conn.meta.userID]) {
                    delete this.userConnections[conn.meta.userID][conn.id];
                }
            });
        });
    }
    sendNotification(userID, type, date, meta) {
        console.log('Send notification to', userID, type, date);
        let connections = this.userConnections[userID];
        let body = {
            meta,
            type,
            date
        };
        for (let cid in connections) {
            connections[cid].write(this.composeMessage('notification', body));
        }
    }
    sendMessage(userID, roomStringID, type, date, meta, text, sender) {
        let connections = this.userConnections[userID];
        let body = {
            meta,
            type,
            date,
            text,
            user: sender
        };
        for (let cid in connections) {
            connections[cid].write(this.composeMessage(`message.${roomStringID}`, body));
        }
    }
    sendMessageUpdate(messageID, userID, roomStringID, text, meta) {
        let connections = this.userConnections[userID];
        let body = {
            meta,
            text,
            id: messageID
        };
        for (let cid in connections) {
            connections[cid].write(this.composeMessage(`message.update.${roomStringID}`, body));
        }
    }
    handleAuthenticatedSocketMessage(connection, message) {
        console.log('got message', message);
        let type = message.t, typeSplitted = type.split('.'), body = message.b, uid = message.u;
        if (typeSplitted[0] === 'history' && typeSplitted[1]) {
            this.handleGetHistory(connection, typeSplitted[1], body, uid);
        }
        if (typeSplitted[0] === 'message' && typeSplitted[1]) {
            this.handleSendMessage(connection, typeSplitted[1], body);
        }
        if (typeSplitted[0] === 'rooms' && typeSplitted[1]) {
            this.handleGetRooms(connection, typeSplitted[1], body, uid);
        }
        if (typeSplitted[0] === 'room' && typeSplitted[1]) {
            this.handleGetRoom(connection, typeSplitted[1], body, uid);
        }
        if (typeSplitted[0] === 'count') {
            this.handleGetCount(connection, body);
        }
        if (typeSplitted[0] === 'archive') {
            this.handleArchiveRoom(connection, body);
        }
    }
    handleGetHistory(connection, roomStringID, body, uid) {
        let fnSendError = (err, statusCode) => {
            winston_1.error(err);
            connection.write(this.composeMessage(`history.${roomStringID}`, { error: { status: statusCode } }, uid));
        };
        utils.checkRoomParticipant(roomStringID, connection.meta.userID).then(allowed => {
            if (!allowed) {
                fnSendError(new Error(`User ${connection.meta.userID} is not allowed to read room ${roomStringID}`), 403);
                return;
            }
            utils.getMessages(roomStringID, body, connection.meta.userID).then(messages => {
                let fnSendMessages = (markAsRead = false) => {
                    connection.write(this.composeMessage(`history.${roomStringID}`, messages, uid));
                    if (markAsRead) {
                        connection.write(this.composeMessage('read', { room: roomStringID }, uid));
                    }
                };
                if (body.markAsRead) {
                    utils.markRoomAsRead(roomStringID, connection.meta.userID).then(fnSendMessages.bind(null, true)).catch(fnSendMessages);
                }
                else {
                    fnSendMessages();
                }
            }).catch(err => fnSendError(err, 500));
        }).catch(err => fnSendError(err, 500));
    }
    handleGetRooms(connection, type, body, uid) {
        let fnSendError = (err, statusCode) => {
            winston_1.error(err);
            connection.write(this.composeMessage(`rooms.${type}`, { error: { status: statusCode } }, uid));
        };
        utils.getRoomsWithCount(type, connection.meta.userID, body).then(data => {
            connection.write(this.composeMessage(`rooms.${type}`, data, uid));
        }).catch(err => fnSendError(err, 500));
    }
    handleGetRoom(connection, roomStringID, body, uid) {
        let fnSendError = (err, statusCode) => {
            winston_1.error(err);
            connection.write(this.composeMessage(`room.${roomStringID}`, { error: { status: statusCode } }, uid));
        };
        utils.checkRoomParticipant(roomStringID, connection.meta.userID).then(allowed => {
            if (!allowed) {
                fnSendError(null, 403);
                return;
            }
            let parts = roomStringID.split(':');
            utils.getRoomComplete(parts[0], +parts[1]).then(room => {
                connection.write(this.composeMessage(`room.${roomStringID}`, room, uid));
            }).catch(err => fnSendError(err, 500));
        }).catch(err => fnSendError(err, 500));
    }
    handleGetCount(connection, body) {
        let fnSendError = (err, statusCode) => {
            winston_1.error(err);
            connection.write(this.composeMessage(`count`, { error: { status: statusCode } }));
        };
        utils.getRoomsCount(body.rooms, connection.meta.userID).then(data => {
            connection.write(this.composeMessage(`count`, data));
        }).catch(err => fnSendError(err, 500));
    }
    handleArchiveRoom(connection, body) {
        let fnSendError = (err, statusCode) => {
            winston_1.error(err);
            connection.write(this.composeMessage(`archive`, { error: { status: statusCode } }));
        };
        utils.archiveRooms(body.rooms, connection.meta.userID).then(rooms => {
            connection.write(this.composeMessage(`archive`, { rooms: rooms }));
        }).catch(err => fnSendError(err, 500));
    }
    handleSendMessage(connection, roomStringID, body) {
        let fnSendError = (statusCode) => {
            connection.write(this.composeMessage(`message.${roomStringID}`, { error: { status: statusCode } }));
        };
        utils.checkRoomParticipant(roomStringID, connection.meta.userID).then(allowed => {
            if (!allowed) {
                fnSendError(403);
                return;
            }
            utils.createMessage(roomStringID, connection.meta.userID, { text: body.text, meta: body.meta, type: 'message' }).then().catch(err => fnSendError(500));
        }).catch(err => fnSendError(500));
    }
    composeMessage(type, body, uid) {
        let msg = { t: type, b: body };
        if (uid) {
            msg.u = uid;
        }
        return JSON.stringify(msg);
    }
    generateAuthToken() {
        return crypto_1.randomBytes(32).toString('hex');
    }
    authenticateConnection(cid, token, data) {
        if (!(cid in this.connections)) {
            return;
        }
        let connection = this.connections[cid], meta = connection.meta;
        if (!meta || !meta.authToken || meta.authToken !== token) {
            connection.close();
            return;
        }
        meta = Object.assign({}, meta, {
            userID: data.userID
        });
        connection.meta = meta;
        if (!this.userConnections[connection.meta.userID]) {
            this.userConnections[connection.meta.userID] = {};
        }
        this.userConnections[connection.meta.userID][cid] = connection;
        connection.write(JSON.stringify({
            t: 'auth.success'
        }));
    }
}
const messaging = new Messaging();
exports.default = messaging;
