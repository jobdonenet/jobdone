"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const middleware_1 = require("./middleware");
class AsyncRouter {
    constructor(isProtected) {
        this.expressRouter = express_1.Router();
        this.isProtected = isProtected;
    }
    applyMethod(method, path, routeFunction) {
        let args = [path];
        if (this.isProtected) {
            args.push(middleware_1.protectRoute);
        }
        args.push((req, res) => {
            routeFunction(req, res)
                .then()
                .catch(middleware_1.sendError.bind(null, res));
        });
        this.expressRouter[method].apply(this.expressRouter, args);
    }
    get(path, routeFunction) {
        return this.applyMethod.call(this, 'get', ...arguments);
    }
    post(path, routeFunction) {
        return this.applyMethod.call(this, 'post', ...arguments);
    }
    put(path, routeFunction) {
        return this.applyMethod.call(this, 'put', ...arguments);
    }
    delete(path, routeFunction) {
        return this.applyMethod.call(this, 'delete', ...arguments);
    }
    getExpressRouter() {
        return this.expressRouter;
    }
}
exports.default = AsyncRouter;
