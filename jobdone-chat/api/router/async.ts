import { Router as ExpressRouter, Request as ExpressRequest, Response as ExpressResponse } from 'express';

import { protectRoute, IPatchedRequest, sendError } from './middleware';


/**
 * Custom router class
 *
 * Provides a patched Express.js router which expects Promise as a route function
 *  and by default provides helper methods and authentication middleware
 */
export default class AsyncRouter {
    private expressRouter: ExpressRouter;
    private isProtected: boolean;

    constructor(isProtected: boolean) {
        this.expressRouter = ExpressRouter();
        this.isProtected = isProtected;
    }

    private applyMethod(method: string, path: string, routeFunction: (req: IPatchedRequest, res: ExpressResponse) => Promise<any>) {
        let args: any[] = [path];
        if (this.isProtected) {
            args.push(protectRoute);
        }

        args.push((req: IPatchedRequest, res: ExpressResponse) => {
            routeFunction(req, res)
                .then()
                .catch(sendError.bind(null, res));
        });

        this.expressRouter[method].apply(this.expressRouter, args);
    }

    get(path: string, routeFunction: (req: IPatchedRequest, res: ExpressResponse) => Promise<any>) {
        return this.applyMethod.call(this, 'get', ...arguments);
    }

    post(path: string, routeFunction: (req: IPatchedRequest, res: ExpressResponse) => Promise<any>) {
        return this.applyMethod.call(this, 'post', ...arguments);
    }

    put(path: string, routeFunction: (req: IPatchedRequest, res: ExpressResponse) => Promise<any>) {
        return this.applyMethod.call(this, 'put', ...arguments);
    }

    delete(path: string, routeFunction: (req: IPatchedRequest, res: ExpressResponse) => Promise<any>) {
        return this.applyMethod.call(this, 'delete', ...arguments);
    }

    getExpressRouter() {
        return this.expressRouter;
    }

}