"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const JWT = require("jsonwebtoken");
const winston_1 = require("winston");
class NotFoundError extends Error {
}
exports.NotFoundError = NotFoundError;
class ForbiddenError extends Error {
}
exports.ForbiddenError = ForbiddenError;
class DuplicateError extends Error {
}
exports.DuplicateError = DuplicateError;
class ValidationError extends Error {
}
exports.ValidationError = ValidationError;
class BadRequestError extends Error {
}
exports.BadRequestError = BadRequestError;
function protectRoute(req, res, next) {
    if (!req.headers.authorization || req.headers.authorization.split(' ')[0] !== 'Bearer') {
        return res.send(401);
    }
    let token = req.headers.authorization.split(' ')[1];
    JWT.verify(token, req.app.get('secret'), (err, data) => {
        if (err) {
            return res.send(401);
        }
        req.user = {
            id: data.userID
        };
        next();
    });
}
exports.protectRoute = protectRoute;
function sendError(res, err) {
    if (err instanceof NotFoundError) {
        return res.status(404).json({
            errors: {
                general: err.message || 'Not found'
            }
        });
    }
    if (err instanceof ForbiddenError) {
        return res.status(403).json({
            errors: {
                general: err.message || 'You are not allowed to perform this operation'
            }
        });
    }
    if (err instanceof DuplicateError) {
        return res.status(409).json({
            errors: {
                general: err.message || 'This entity already exists'
            }
        });
    }
    if (err instanceof BadRequestError) {
        return res.status(400).json({
            errors: {
                general: err.message || 'Incoming data has a wrong format'
            }
        });
    }
    winston_1.error('Internal server error:', err);
    return res.status(500).json({
        errors: {
            general: err.message || 'Internal server error has occured'
        }
    });
}
exports.sendError = sendError;
