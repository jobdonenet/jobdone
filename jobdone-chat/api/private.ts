import { info, error, debug } from 'winston';

import db from '../db';
import messaging from './messaging';
import AsyncRouter from './router/async';
import { BadRequestError } from './router/middleware';


const asyncRouter = new AsyncRouter(false);


const NOTIFICATION_ROOM_TYPE = 'notification';
const ORDER_ROOM_TYPE = 'order';
const ENQUIRY_ROOM_TYPE = 'enquiry';


asyncRouter.post('/connection/auth', async (req, res) => {
    let { cid, token, data } = req.body;

    if (!cid || !token || !data) {
        throw new BadRequestError('Missing either cid or token or data');
    }

    messaging.authenticateConnection(cid, token, data);

    res.json({});
});


asyncRouter.post('/notification', async (req, res) => {
    let notifications = [];

    if (Array.isArray(req.body)) {
        notifications = req.body;
    } else {
        notifications.push(req.body);
    }

    for (let notification of notifications) {
        let room = await getRoom(NOTIFICATION_ROOM_TYPE, notification.userID),
            roomStringID = [NOTIFICATION_ROOM_TYPE, notification.userID].join(':');

        if (!room) {
            debug(`No room '${roomStringID}' found, creating new one`);

            await db.query('INSERT INTO rooms (type, entity_id) VALUES (?, ?)', [ NOTIFICATION_ROOM_TYPE, notification.userID ]);

            room = await getRoom(NOTIFICATION_ROOM_TYPE, notification.userID);

            // Add user to participants of his notification room
            await db.query('INSERT INTO participants (user_id, room_id) VALUES (?, ?)', [ notification.userID, room.id ]);
        }

        debug(`Got room '${roomStringID}':`, room);

        await createNotification(notification.userID, room.id, notification.type, notification.meta);
    }

    res.json({});
});


asyncRouter.post('/message/order', async (req, res) => {
    let messages = [];

    if (Array.isArray(req.body)) {
        messages = req.body;
    } else {
        messages.push(req.body);
    }

    for (let message of messages) {
        let room = await getRoom(ORDER_ROOM_TYPE, message.orderID),
            roomStringID = [ORDER_ROOM_TYPE, message.orderID].join(':'),
            senderID = message.userID ? message.userID : null;

        if (!room) {
            debug(`No room '${roomStringID}' found, creating new one`);

            let roomMetaJSON = message.meta && message.meta.room ? JSON.stringify(message.meta.room) : null;

            await db.query('INSERT INTO rooms (type, entity_id, meta_json) VALUES (?, ?, ?)', [ ORDER_ROOM_TYPE, message.orderID, roomMetaJSON ]);

            room = await getRoom(ORDER_ROOM_TYPE, message.orderID);

            // Add both seller and buyer to participants of his notification room
            for (let userID of message.users) {
                await db.query('INSERT INTO participants (user_id, room_id) VALUES (?, ?)', [ userID, room.id ]);
            }
        }

        debug(`Got room '${roomStringID}':`, room);

        let messageMeta = message.meta && message.meta.message ? message.meta.message : null,
            messageText = message.text ? message.text : null;

        await createMessage(room.id, roomStringID, message.type, messageMeta, messageText, senderID);
    }

    res.json({});
});


asyncRouter.post('/message/enquiry', async (req, res) => {
    let messages = [];

    if (Array.isArray(req.body)) {
        messages = req.body;
    } else {
        messages.push(req.body);
    }

    for (let message of messages) {
        let room = await getRoom(ENQUIRY_ROOM_TYPE, message.enquiryID, message.users),
            roomStringID = [ENQUIRY_ROOM_TYPE, message.enquiryID].join(':'),
            senderID = message.userID ? message.userID : null;

        if (!room) {
            debug(`No room '${roomStringID}' found, creating new one`);

            let roomMetaJSON = message.meta && message.meta.room ? JSON.stringify(message.meta.room) : null;

            await db.query('INSERT INTO rooms (type, entity_id, meta_json) VALUES (?, ?, ?)', [ ENQUIRY_ROOM_TYPE, message.enquiryID, roomMetaJSON ]);

            room = await getRoom(ENQUIRY_ROOM_TYPE, message.enquiryID);

            // Add both seller and buyer to participants of his notification room
            for (let userID of message.users) {
                await db.query('INSERT INTO participants (user_id, room_id) VALUES (?, ?)', [ userID, room.id ]);
            }
        }

        debug(`Got room '${roomStringID}':`, room);

        let messageMeta = message.meta && message.meta.message ? message.meta.message : null;

        await createMessage(room.id, roomStringID, message.type ? message.type : 'message', messageMeta, message.body.text, senderID);
    }

    res.json({});
});


asyncRouter.post('/order/deliverable_vote', async (req, res) => {
    if (!req.body.orderID || !req.body.deliverableID || +req.body.rating < 1 || +req.body.rating > 5) {
        return res.status(400).send({});
    }

    let room = await getRoom(ORDER_ROOM_TYPE, req.body.orderID),
        roomStringID = [ORDER_ROOM_TYPE, req.body.orderID].join(':');

    if (!room) {
        return res.status(404).send({});
    }

    // TODO: optimize this by using JSON_EXTRACT (available in MySQL 5.7 and later)
    let messages = await db.query(
        'SELECT id, meta_json FROM messages WHERE type = ? AND room_id = ? ORDER BY id DESC',
        [ 'order_sent', room.id ]
    );

    for (let message of messages) {
        let messageMeta;
        
        try {
            messageMeta = JSON.parse(message.meta_json);
            if (messageMeta.deliverable.id !== req.body.deliverableID) {
                continue;
            }
        } catch {
            continue;
        }

        messageMeta.deliverable.rating = Math.trunc(req.body.rating);

        await db.query('UPDATE messages SET meta_json = ? WHERE id = ?', [ JSON.stringify(messageMeta), message.id ]);

        let participants = await db.query('SELECT user_id FROM participants WHERE room_id = ?', [ room.id ]);

        for (let participant of participants) {
            messaging.sendMessageUpdate(message.id, participant.user_id, roomStringID, message.text, messageMeta);
        }

        res.send({});
        break;
    }

    return res.status(404).send({});
});


asyncRouter.post('/order/offer_update', async (req, res) => {
    if (!req.body.orderID || !req.body.orderOfferID || (!req.body.is_closed && !req.body.is_accepted)) {
        return res.status(400).send({});
    }

    let room = await getRoom(ORDER_ROOM_TYPE, req.body.orderID),
        roomStringID = [ORDER_ROOM_TYPE, req.body.orderID].join(':');

    if (!room) {
        return res.status(404).send({});
    }

    // TODO: optimize this by using JSON_EXTRACT (available in MySQL 5.7 and later)
    let messages = await db.query(
        'SELECT id, meta_json FROM messages WHERE type = ? AND room_id = ? ORDER BY id DESC',
        [ 'order_offer', room.id ]
    );

    for (let message of messages) {
        let messageMeta;
        
        try {
            messageMeta = JSON.parse(message.meta_json);
            if (messageMeta.order_offer.id !== req.body.orderOfferID) {
                continue;
            }
        } catch {
            continue;
        }

        if (req.body.is_closed) {
            messageMeta.order_offer.is_closed = true;
        }

        if (req.body.is_accepted) {
            messageMeta.order_offer.is_accepted = true;
        }

        await db.query('UPDATE messages SET meta_json = ? WHERE id = ?', [ JSON.stringify(messageMeta), message.id ]);

        let participants = await db.query('SELECT user_id FROM participants WHERE room_id = ?', [ room.id ]);

        for (let participant of participants) {
            messaging.sendMessageUpdate(message.id, participant.user_id, roomStringID, message.text, messageMeta);
        }

        res.send({});
        return;
    }

    return res.status(404).send({});
});


asyncRouter.post('/enquiry/offer_update', async (req, res) => {
    if (!req.body.enquiryID || !req.body.enquiryOfferID || (!req.body.is_closed && !req.body.is_accepted)) {
        return res.status(400).send({});
    }

    let room = await getRoom(ENQUIRY_ROOM_TYPE, req.body.enquiryID),
        roomStringID = [ENQUIRY_ROOM_TYPE, req.body.enquiryID].join(':');

    if (!room) {
        return res.status(404).send({});
    }

    // TODO: optimize this by using JSON_EXTRACT (available in MySQL 5.7 and later)
    let messages = await db.query(
        'SELECT id, meta_json FROM messages WHERE type = ? AND room_id = ? ORDER BY id DESC',
        [ 'enquiry_offer', room.id ]
    );

    for (let message of messages) {
        let messageMeta;
        
        try {
            messageMeta = JSON.parse(message.meta_json);
            if (messageMeta.enquiry_offer.id !== req.body.enquiryOfferID) {
                continue;
            }
        } catch {
            continue;
        }

        if (req.body.is_closed) {
            messageMeta.enquiry_offer.is_closed = true;
        }

        if (req.body.is_accepted) {
            messageMeta.enquiry_offer.is_accepted = true;
        }

        await db.query('UPDATE messages SET meta_json = ? WHERE id = ?', [ JSON.stringify(messageMeta), message.id ]);

        let participants = await db.query('SELECT user_id FROM participants WHERE room_id = ?', [ room.id ]);

        for (let participant of participants) {
            messaging.sendMessageUpdate(message.id, participant.user_id, roomStringID, message.text, messageMeta);
        }

        res.send({});
        return;
    }

    return res.status(404).send({});
});


/**
 * Request participants and their unread messages
 * If there is no unread messages which were sent 1+ hour ago, do not return anything
 * If there is unread messages which were sent 1+ hour ago, and fresh ones, send all of them
 **/
asyncRouter.post('/participants/unread_messages', async (req, res) => {
    let roomTypes = [ENQUIRY_ROOM_TYPE, ORDER_ROOM_TYPE];

    let participants = await db.query(`
        SELECT
            p.id as participant_id,
            p.user_id as recipient_id,
            r.id as room_id,
            m.user_id as sender_id,
            r.type as type,
            r.entity_id as entity_id,
            (
                SELECT COUNT(*)
                FROM messages m2
                WHERE room_id = r.id
                AND m2.type = ?
                AND m2.user_id != p.user_id
                AND (p.last_notified_message_id IS NOT NULL AND p.last_notified_message_id < m.id OR p.last_notified_message_id IS NULL)
            ) as message_count,
            MAX(m.id) as message_id
        FROM messages m, rooms r, participants p
        WHERE m.room_id = r.id
        AND p.room_id = r.id
        AND m.type = ?
        AND m.user_id != p.user_id
        AND r.type IN (?)
        AND (p.last_notified_message_id IS NOT NULL AND p.last_notified_message_id < m.id OR p.last_notified_message_id IS NULL)
        AND m.create_date < DATE_SUB(NOW(), INTERVAL 10 MINUTE)
        GROUP BY p.id;
    `, ['message', 'message', roomTypes]);

    // db.query(`
    //     UPDATE participants p, rooms r, messages m
    //     SET p.last_notified_message_id = (
    //         SELECT MAX(id)
    //         FROM messages m2
    //         WHERE room_id = r.id
    //         AND m2.type = ?
    //         AND m2.user_id != p.user_id
    //         AND p.last_notified_message_id IS NOT NULL
    //         AND p.last_notified_message_id < m2.id
    //     )
    //     WHERE m.room_id = r.id
    //     AND p.room_id = r.id
    //     AND m.type = ?
    //     AND m.user_id != p.user_id
    //     AND r.type IN (?)
    //     AND p.last_notified_message_id IS NOT NULL
    //     AND p.last_notified_message_id < m.id
    //     AND m.create_date < DATE_SUB(NOW(), INTERVAL 1 HOUR);
    // `, ['message', 'message', roomTypes]);

    let result = {
        participants: []
    };

    for (let participant of participants) {
        result.participants.push({
            recipient_id: participant.recipient_id,
            sender_id: participant.sender_id,
            count: participant.message_count,
            type: participant.type,
            entity_id: participant.entity_id
        });

        await db.query(
            'UPDATE participants SET last_notified_message_id = ? WHERE id = ?',
            [participant.message_id, participant.participant_id]
        );
    }

    res.json(result);
});


async function getRoom(type: string, entityID: number, participants?: number[]) {
    if (participants) {
        return await db.queryOne('SELECT r.id FROM rooms r, participants p WHERE r.type = ? AND r.entity_id = ? AND p.room_id = r.id AND p.user_id IN (?)', [ type, entityID, participants ]);
    }

    return await db.queryOne('SELECT id FROM rooms WHERE type = ? AND entity_id = ?', [ type, entityID ]);
}


async function createNotification(userID: number, roomID: number, type: string, meta: any) {
    let metaJSON = JSON.stringify(meta);
    
    let messageID = await db.insertOne('INSERT INTO messages (room_id, type, meta_json) VALUES (?, ?, ?)', [ roomID, type, metaJSON ]),
        message = await db.queryOne('SELECT id, create_date FROM messages WHERE id = ?', [ messageID ]);

    await db.query('UPDATE rooms SET is_empty = 0, last_action_date = ?, last_message_id = ? WHERE id = ?', [ message.create_date, messageID, roomID ]);

    messaging.sendNotification(userID, type, message.create_date, meta);
}


async function createMessage(roomID: number, roomStringID: string, type: string, meta: any, text?: string, senderID?: number) {
    let metaJSON = JSON.stringify(meta),
        sender = senderID ? senderID : null,
        messageText = text ? text : null;
    
    let messageID = await db.insertOne('INSERT INTO messages (room_id, type, meta_json, message, user_id) VALUES (?, ?, ?, ?, ?)', [ roomID, type, metaJSON, text, sender ]),
        message = await db.queryOne('SELECT id, create_date FROM messages WHERE id = ?', [ messageID ]);

    await db.query('UPDATE rooms SET is_empty = 0, last_action_date = ?, last_message_id = ? WHERE id = ?', [ message.create_date, messageID, roomID ]);

    let participants = await db.query('SELECT user_id FROM participants WHERE room_id = ?', [ roomID ]);

    if (sender) {
        // Update last read message for sender
        await db.query('UPDATE participants SET last_read_message_id = ?, last_notified_message_id = ? WHERE room_id = ? AND user_id = ?', [ messageID, messageID, roomID, sender ]);
    }

    for (let participant of participants) {
        messaging.sendMessage(participant.user_id, roomStringID, type, message.create_date, meta, messageText, senderID);
    }
}


module.exports = asyncRouter.getExpressRouter();
