"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = require("winston");
const db_1 = require("../db");
const messaging_1 = require("./messaging");
const async_1 = require("./router/async");
const middleware_1 = require("./router/middleware");
const asyncRouter = new async_1.default(false);
const NOTIFICATION_ROOM_TYPE = 'notification';
const ORDER_ROOM_TYPE = 'order';
const ENQUIRY_ROOM_TYPE = 'enquiry';
asyncRouter.post('/connection/auth', (req, res) => __awaiter(this, void 0, void 0, function* () {
    let { cid, token, data } = req.body;
    if (!cid || !token || !data) {
        throw new middleware_1.BadRequestError('Missing either cid or token or data');
    }
    messaging_1.default.authenticateConnection(cid, token, data);
    res.json({});
}));
asyncRouter.post('/notification', (req, res) => __awaiter(this, void 0, void 0, function* () {
    let notifications = [];
    if (Array.isArray(req.body)) {
        notifications = req.body;
    }
    else {
        notifications.push(req.body);
    }
    for (let notification of notifications) {
        let room = yield getRoom(NOTIFICATION_ROOM_TYPE, notification.userID), roomStringID = [NOTIFICATION_ROOM_TYPE, notification.userID].join(':');
        if (!room) {
            winston_1.debug(`No room '${roomStringID}' found, creating new one`);
            yield db_1.default.query('INSERT INTO rooms (type, entity_id) VALUES (?, ?)', [NOTIFICATION_ROOM_TYPE, notification.userID]);
            room = yield getRoom(NOTIFICATION_ROOM_TYPE, notification.userID);
            yield db_1.default.query('INSERT INTO participants (user_id, room_id) VALUES (?, ?)', [notification.userID, room.id]);
        }
        winston_1.debug(`Got room '${roomStringID}':`, room);
        yield createNotification(notification.userID, room.id, notification.type, notification.meta);
    }
    res.json({});
}));
asyncRouter.post('/message/order', (req, res) => __awaiter(this, void 0, void 0, function* () {
    let messages = [];
    if (Array.isArray(req.body)) {
        messages = req.body;
    }
    else {
        messages.push(req.body);
    }
    for (let message of messages) {
        let room = yield getRoom(ORDER_ROOM_TYPE, message.orderID), roomStringID = [ORDER_ROOM_TYPE, message.orderID].join(':'), senderID = message.userID ? message.userID : null;
        if (!room) {
            winston_1.debug(`No room '${roomStringID}' found, creating new one`);
            let roomMetaJSON = message.meta && message.meta.room ? JSON.stringify(message.meta.room) : null;
            yield db_1.default.query('INSERT INTO rooms (type, entity_id, meta_json) VALUES (?, ?, ?)', [ORDER_ROOM_TYPE, message.orderID, roomMetaJSON]);
            room = yield getRoom(ORDER_ROOM_TYPE, message.orderID);
            for (let userID of message.users) {
                yield db_1.default.query('INSERT INTO participants (user_id, room_id) VALUES (?, ?)', [userID, room.id]);
            }
        }
        winston_1.debug(`Got room '${roomStringID}':`, room);
        let messageMeta = message.meta && message.meta.message ? message.meta.message : null, messageText = message.text ? message.text : null;
        yield createMessage(room.id, roomStringID, message.type, messageMeta, messageText, senderID);
    }
    res.json({});
}));
asyncRouter.post('/message/enquiry', (req, res) => __awaiter(this, void 0, void 0, function* () {
    let messages = [];
    if (Array.isArray(req.body)) {
        messages = req.body;
    }
    else {
        messages.push(req.body);
    }
    for (let message of messages) {
        let room = yield getRoom(ENQUIRY_ROOM_TYPE, message.enquiryID, message.users), roomStringID = [ENQUIRY_ROOM_TYPE, message.enquiryID].join(':'), senderID = message.userID ? message.userID : null;
        if (!room) {
            winston_1.debug(`No room '${roomStringID}' found, creating new one`);
            let roomMetaJSON = message.meta && message.meta.room ? JSON.stringify(message.meta.room) : null;
            yield db_1.default.query('INSERT INTO rooms (type, entity_id, meta_json) VALUES (?, ?, ?)', [ENQUIRY_ROOM_TYPE, message.enquiryID, roomMetaJSON]);
            room = yield getRoom(ENQUIRY_ROOM_TYPE, message.enquiryID);
            for (let userID of message.users) {
                yield db_1.default.query('INSERT INTO participants (user_id, room_id) VALUES (?, ?)', [userID, room.id]);
            }
        }
        winston_1.debug(`Got room '${roomStringID}':`, room);
        let messageMeta = message.meta && message.meta.message ? message.meta.message : null;
        yield createMessage(room.id, roomStringID, message.type ? message.type : 'message', messageMeta, message.body.text, senderID);
    }
    res.json({});
}));
asyncRouter.post('/order/deliverable_vote', (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (!req.body.orderID || !req.body.deliverableID || +req.body.rating < 1 || +req.body.rating > 5) {
        return res.status(400).send({});
    }
    let room = yield getRoom(ORDER_ROOM_TYPE, req.body.orderID), roomStringID = [ORDER_ROOM_TYPE, req.body.orderID].join(':');
    if (!room) {
        return res.status(404).send({});
    }
    let messages = yield db_1.default.query('SELECT id, meta_json FROM messages WHERE type = ? AND room_id = ? ORDER BY id DESC', ['order_sent', room.id]);
    for (let message of messages) {
        let messageMeta;
        try {
            messageMeta = JSON.parse(message.meta_json);
            if (messageMeta.deliverable.id !== req.body.deliverableID) {
                continue;
            }
        }
        catch (_a) {
            continue;
        }
        messageMeta.deliverable.rating = Math.trunc(req.body.rating);
        yield db_1.default.query('UPDATE messages SET meta_json = ? WHERE id = ?', [JSON.stringify(messageMeta), message.id]);
        let participants = yield db_1.default.query('SELECT user_id FROM participants WHERE room_id = ?', [room.id]);
        for (let participant of participants) {
            messaging_1.default.sendMessageUpdate(message.id, participant.user_id, roomStringID, message.text, messageMeta);
        }
        res.send({});
        break;
    }
    return res.status(404).send({});
}));
asyncRouter.post('/order/offer_update', (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (!req.body.orderID || !req.body.orderOfferID || (!req.body.is_closed && !req.body.is_accepted)) {
        return res.status(400).send({});
    }
    let room = yield getRoom(ORDER_ROOM_TYPE, req.body.orderID), roomStringID = [ORDER_ROOM_TYPE, req.body.orderID].join(':');
    if (!room) {
        return res.status(404).send({});
    }
    let messages = yield db_1.default.query('SELECT id, meta_json FROM messages WHERE type = ? AND room_id = ? ORDER BY id DESC', ['order_offer', room.id]);
    for (let message of messages) {
        let messageMeta;
        try {
            messageMeta = JSON.parse(message.meta_json);
            if (messageMeta.order_offer.id !== req.body.orderOfferID) {
                continue;
            }
        }
        catch (_b) {
            continue;
        }
        if (req.body.is_closed) {
            messageMeta.order_offer.is_closed = true;
        }
        if (req.body.is_accepted) {
            messageMeta.order_offer.is_accepted = true;
        }
        yield db_1.default.query('UPDATE messages SET meta_json = ? WHERE id = ?', [JSON.stringify(messageMeta), message.id]);
        let participants = yield db_1.default.query('SELECT user_id FROM participants WHERE room_id = ?', [room.id]);
        for (let participant of participants) {
            messaging_1.default.sendMessageUpdate(message.id, participant.user_id, roomStringID, message.text, messageMeta);
        }
        res.send({});
        return;
    }
    return res.status(404).send({});
}));
asyncRouter.post('/enquiry/offer_update', (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (!req.body.enquiryID || !req.body.enquiryOfferID || (!req.body.is_closed && !req.body.is_accepted)) {
        return res.status(400).send({});
    }
    let room = yield getRoom(ENQUIRY_ROOM_TYPE, req.body.enquiryID), roomStringID = [ENQUIRY_ROOM_TYPE, req.body.enquiryID].join(':');
    if (!room) {
        return res.status(404).send({});
    }
    let messages = yield db_1.default.query('SELECT id, meta_json FROM messages WHERE type = ? AND room_id = ? ORDER BY id DESC', ['enquiry_offer', room.id]);
    for (let message of messages) {
        let messageMeta;
        try {
            messageMeta = JSON.parse(message.meta_json);
            if (messageMeta.enquiry_offer.id !== req.body.enquiryOfferID) {
                continue;
            }
        }
        catch (_c) {
            continue;
        }
        if (req.body.is_closed) {
            messageMeta.enquiry_offer.is_closed = true;
        }
        if (req.body.is_accepted) {
            messageMeta.enquiry_offer.is_accepted = true;
        }
        yield db_1.default.query('UPDATE messages SET meta_json = ? WHERE id = ?', [JSON.stringify(messageMeta), message.id]);
        let participants = yield db_1.default.query('SELECT user_id FROM participants WHERE room_id = ?', [room.id]);
        for (let participant of participants) {
            messaging_1.default.sendMessageUpdate(message.id, participant.user_id, roomStringID, message.text, messageMeta);
        }
        res.send({});
        return;
    }
    return res.status(404).send({});
}));
asyncRouter.post('/participants/unread_messages', (req, res) => __awaiter(this, void 0, void 0, function* () {
    let roomTypes = [ENQUIRY_ROOM_TYPE, ORDER_ROOM_TYPE];
    let participants = yield db_1.default.query(`
        SELECT
            p.id as participant_id,
            p.user_id as recipient_id,
            r.id as room_id,
            m.user_id as sender_id,
            r.type as type,
            r.entity_id as entity_id,
            (
                SELECT COUNT(*)
                FROM messages m2
                WHERE room_id = r.id
                AND m2.type = ?
                AND m2.user_id != p.user_id
                AND (p.last_notified_message_id IS NOT NULL AND p.last_notified_message_id < m.id OR p.last_notified_message_id IS NULL)
            ) as message_count,
            MAX(m.id) as message_id
        FROM messages m, rooms r, participants p
        WHERE m.room_id = r.id
        AND p.room_id = r.id
        AND m.type = ?
        AND m.user_id != p.user_id
        AND r.type IN (?)
        AND (p.last_notified_message_id IS NOT NULL AND p.last_notified_message_id < m.id OR p.last_notified_message_id IS NULL)
        AND m.create_date < DATE_SUB(NOW(), INTERVAL 10 MINUTE)
        GROUP BY p.id;
    `, ['message', 'message', roomTypes]);
    let result = {
        participants: []
    };
    for (let participant of participants) {
        result.participants.push({
            recipient_id: participant.recipient_id,
            sender_id: participant.sender_id,
            count: participant.message_count,
            type: participant.type,
            entity_id: participant.entity_id
        });
        yield db_1.default.query('UPDATE participants SET last_notified_message_id = ? WHERE id = ?', [participant.message_id, participant.participant_id]);
    }
    res.json(result);
}));
function getRoom(type, entityID, participants) {
    return __awaiter(this, void 0, void 0, function* () {
        if (participants) {
            return yield db_1.default.queryOne('SELECT r.id FROM rooms r, participants p WHERE r.type = ? AND r.entity_id = ? AND p.room_id = r.id AND p.user_id IN (?)', [type, entityID, participants]);
        }
        return yield db_1.default.queryOne('SELECT id FROM rooms WHERE type = ? AND entity_id = ?', [type, entityID]);
    });
}
function createNotification(userID, roomID, type, meta) {
    return __awaiter(this, void 0, void 0, function* () {
        let metaJSON = JSON.stringify(meta);
        let messageID = yield db_1.default.insertOne('INSERT INTO messages (room_id, type, meta_json) VALUES (?, ?, ?)', [roomID, type, metaJSON]), message = yield db_1.default.queryOne('SELECT id, create_date FROM messages WHERE id = ?', [messageID]);
        yield db_1.default.query('UPDATE rooms SET is_empty = 0, last_action_date = ?, last_message_id = ? WHERE id = ?', [message.create_date, messageID, roomID]);
        messaging_1.default.sendNotification(userID, type, message.create_date, meta);
    });
}
function createMessage(roomID, roomStringID, type, meta, text, senderID) {
    return __awaiter(this, void 0, void 0, function* () {
        let metaJSON = JSON.stringify(meta), sender = senderID ? senderID : null, messageText = text ? text : null;
        let messageID = yield db_1.default.insertOne('INSERT INTO messages (room_id, type, meta_json, message, user_id) VALUES (?, ?, ?, ?, ?)', [roomID, type, metaJSON, text, sender]), message = yield db_1.default.queryOne('SELECT id, create_date FROM messages WHERE id = ?', [messageID]);
        yield db_1.default.query('UPDATE rooms SET is_empty = 0, last_action_date = ?, last_message_id = ? WHERE id = ?', [message.create_date, messageID, roomID]);
        let participants = yield db_1.default.query('SELECT user_id FROM participants WHERE room_id = ?', [roomID]);
        if (sender) {
            yield db_1.default.query('UPDATE participants SET last_read_message_id = ?, last_notified_message_id = ? WHERE room_id = ? AND user_id = ?', [messageID, messageID, roomID, sender]);
        }
        for (let participant of participants) {
            messaging_1.default.sendMessage(participant.user_id, roomStringID, type, message.create_date, meta, messageText, senderID);
        }
    });
}
module.exports = asyncRouter.getExpressRouter();
