"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = require("winston");
const messaging_1 = require("./messaging");
const db_1 = require("../db");
const NOTIFICATION_ROOM_TYPE = 'notification';
const FOLDER_LIST = [
    'inbox',
    'unread',
    'sent',
    'archive'
];
function checkRoomParticipant(roomStringID, userID) {
    return __awaiter(this, void 0, void 0, function* () {
        let [roomType, roomEntityID] = roomStringID.split(':');
        if (!roomType || !roomEntityID) {
            throw new Error('Wrong room ID');
        }
        let allowed = !!(yield db_1.default.queryOne('SELECT p.id FROM participants p, rooms r WHERE p.room_id = r.id AND r.type = ? AND r.entity_id = ? AND p.user_id = ?', [roomType, roomEntityID, userID]));
        if (!allowed && roomType === NOTIFICATION_ROOM_TYPE && !isNaN(+roomEntityID) && +roomEntityID === userID) {
            return true;
        }
        return allowed;
    });
}
exports.checkRoomParticipant = checkRoomParticipant;
function markRoomAsRead(roomStringID, userID) {
    return __awaiter(this, void 0, void 0, function* () {
        let [roomType, roomEntityID] = roomStringID.split(':');
        if (!roomType || !roomEntityID) {
            throw new Error('Wrong room ID');
        }
        yield db_1.default.query('UPDATE participants p, rooms r SET p.last_notified_message_id = (SELECT MAX(id) FROM messages WHERE room_id = r.id), p.last_read_message_id = p.last_notified_message_id WHERE p.room_id = r.id AND p.user_id = ? AND r.type = ? AND r.entity_id = ?', [userID, roomType, roomEntityID]);
    });
}
exports.markRoomAsRead = markRoomAsRead;
function getMessages(roomStringID, params, userID) {
    return __awaiter(this, void 0, void 0, function* () {
        let [roomType, roomEntityID] = roomStringID.split(':');
        if (!roomType || !roomEntityID) {
            throw new Error('Wrong room ID');
        }
        let offset = params && !isNaN(params.offset) ? params.offset : 0, limit = params && !isNaN(params.limit) ? params.limit : 4294967295;
        let messages = yield db_1.default.query('SELECT m.* FROM messages m, rooms r WHERE m.room_id = r.id AND r.type = ? AND r.entity_id = ? ORDER BY m.id DESC LIMIT ?, ?', [roomType, roomEntityID, offset, limit]);
        let total = (yield db_1.default.queryOne('SELECT count(*) as count FROM messages m, rooms r WHERE m.room_id = r.id AND r.type = ? AND r.entity_id = ?;', [roomType, roomEntityID])).count;
        let unread = (yield db_1.default.queryOne('SELECT count(*) as count FROM messages m, rooms r, participants p WHERE m.room_id = r.id AND p.room_id = r.id AND r.type = ? AND r.entity_id = ? AND (p.last_read_message_id IS NULL OR p.last_read_message_id < m.id);', [roomType, roomEntityID])).count;
        messages = messages.map(message => {
            return {
                id: message.id,
                date: message.create_date,
                meta: message.meta_json ? JSON.parse(message.meta_json) : null,
                type: message.type,
                user: message.user_id,
                text: message.message
            };
        });
        return {
            data: messages,
            meta: {
                total: total,
                unread: unread
            }
        };
    });
}
exports.getMessages = getMessages;
function getRoomsWithCount(type, userID, params) {
    return __awaiter(this, void 0, void 0, function* () {
        let offset = params && !isNaN(params.offset) ? params.offset : 0, limit = params && !isNaN(params.limit) ? params.limit : 4294967295, folder = params && params.folder && FOLDER_LIST.indexOf(params.folder) !== -1 ? params.folder : null, rooms;
        let typeRequested = [type];
        if (type.indexOf(',') !== -1) {
            typeRequested = type.split(',');
        }
        let isUnreadSQL = (folder === 'unread' ? 'HAVING unread' : ''), isArchivedSQL = (folder === 'archive' ? 'AND p.is_archived' : 'AND NOT p.is_archived'), messageOriginSQL = '';
        if (folder === 'inbox' || folder === 'unread') {
            messageOriginSQL = 'AND r.last_message_id IS NOT NULL AND m.id = r.last_message_id AND (m.user_id != p.user_id OR EXISTS (SELECT COUNT(DISTINCT(m2.user_id)) as m2cnt FROM messages m2 WHERE m2.room_id = r.id HAVING m2cnt = 2))';
        }
        else if (folder === 'sent') {
            messageOriginSQL = 'AND r.last_message_id IS NOT NULL AND m.id = r.last_message_id AND m.user_id = p.user_id AND EXISTS (SELECT COUNT(DISTINCT(m2.user_id)) as m2cnt FROM messages m2 WHERE m2.room_id = r.id HAVING m2cnt = 1)';
        }
        rooms = yield db_1.default.query(`
        SELECT
            r.*,
            (p.last_read_message_id IS NULL OR p.last_read_message_id < r.last_message_id) AS unread,
            CASE WHEN LENGTH(m.message) > 100 THEN CONCAT(SUBSTRING(m.message, 1, 100), '...') ELSE m.message END as message,
            p.is_archived
        FROM
            rooms r,
            participants p,
            messages m
        WHERE
            p.user_id = ?
            AND r.id = p.room_id
            AND r.type IN (?)
            ${messageOriginSQL}
            ${isArchivedSQL}
        GROUP BY r.id
        ${isUnreadSQL}
        ORDER BY
            r.last_action_date DESC,
            r.id DESC
        LIMIT ?, ?;`, [userID, typeRequested, offset, limit]);
        rooms = rooms.map(room => {
            if (room.meta_json) {
                room.meta = JSON.parse(room.meta_json);
                delete room.meta_json;
            }
            return room;
        });
        let total = (yield db_1.default.queryOne(`SELECT count(*) as count FROM rooms r, participants p, messages m WHERE p.user_id = ? AND r.id = p.room_id AND r.type IN (?) ${messageOriginSQL} ${isArchivedSQL};`, [userID, typeRequested])).count;
        let unread = (yield db_1.default.queryOne(`SELECT COUNT(*) as count FROM rooms r, participants p, messages m WHERE p.user_id = ? AND r.id = p.room_id AND r.type IN (?) AND (p.last_read_message_id IS NULL OR p.last_read_message_id < r.last_message_id) ${messageOriginSQL} ${isArchivedSQL};`, [userID, typeRequested])).count;
        return {
            data: rooms,
            meta: {
                total: total,
                unread: unread
            }
        };
    });
}
exports.getRoomsWithCount = getRoomsWithCount;
function getRoomsCount(rooms, userID) {
    return __awaiter(this, void 0, void 0, function* () {
        let result = [];
        for (let room of rooms) {
            let total = (yield db_1.default.queryOne('SELECT count(*) as count FROM rooms r, participants p WHERE p.user_id = ? AND r.id = p.room_id AND r.type = ?;', [userID, room])).count;
            let unread = (yield db_1.default.queryOne('SELECT COUNT(*) as count FROM rooms r, participants p WHERE p.user_id = ? AND r.id = p.room_id AND r.type = ? AND (p.last_read_message_id IS NULL OR p.last_read_message_id < r.last_message_id);', [userID, room])).count;
            result.push({ total, unread });
        }
        return {
            data: result
        };
    });
}
exports.getRoomsCount = getRoomsCount;
function archiveRooms(roomStringIDs, userID) {
    return __awaiter(this, void 0, void 0, function* () {
        let result = [];
        for (let roomStringID of roomStringIDs) {
            let [roomType, roomEntityID] = roomStringID.split(':');
            yield db_1.default.query(`
            UPDATE rooms r, participants p
            SET p.is_archived = 1
            WHERE
                p.user_id = ?
                AND r.type = ?
                AND r.entity_id = ?
                AND p.room_id = r.id
        `, [userID, roomType, roomEntityID]);
        }
        return roomStringIDs;
    });
}
exports.archiveRooms = archiveRooms;
function createMessage(roomStringID, senderID, message) {
    return __awaiter(this, void 0, void 0, function* () {
        let [roomType, roomEntityID] = roomStringID.split(':');
        if (!roomType || !roomEntityID) {
            throw new Error('Wrong room ID');
        }
        let room = yield getRoom(roomType, +roomEntityID);
        if (!room) {
            throw new Error('Room not found');
        }
        let metaJSON = message.meta ? JSON.stringify(message.meta) : null, sender = senderID ? senderID : null, text = message.text ? message.text : null;
        let messageID = yield db_1.default.insertOne('INSERT INTO messages (room_id, type, meta_json, message, user_id) VALUES (?, ?, ?, ?, ?)', [room.id, message.type, metaJSON, text, sender]), msg = yield db_1.default.queryOne('SELECT id, create_date FROM messages WHERE id = ?', [messageID]);
        yield db_1.default.query('UPDATE rooms SET is_empty = 0, last_action_date = ?, last_message_id = ? WHERE id = ?', [msg.create_date, messageID, room.id]);
        let participants = yield db_1.default.query('SELECT user_id FROM participants WHERE room_id = ?', [room.id]);
        if (sender) {
            yield db_1.default.query('UPDATE participants SET last_read_message_id = ?, last_notified_message_id = ? WHERE room_id = ? AND user_id = ?', [messageID, messageID, room.id, sender]);
            if (message.meta && message.meta.notify) {
                let otherParticipants = participants.filter(participant => participant.user_id !== sender);
                for (let participant of otherParticipants) {
                    createNewMessageNotification(participant.user_id, roomStringID, { type: roomType, entityId: +roomEntityID, text: text });
                }
            }
        }
        for (let participant of participants) {
            messaging_1.default.sendMessage(participant.user_id, roomStringID, message.type, msg.create_date, message.meta, message.text, sender);
        }
    });
}
exports.createMessage = createMessage;
function createNewMessageNotification(userID, roomStringID, meta) {
    return __awaiter(this, void 0, void 0, function* () {
        let room = yield getRoom(NOTIFICATION_ROOM_TYPE, userID), notificationRoomStringID = [NOTIFICATION_ROOM_TYPE, userID].join(':');
        if (!room) {
            winston_1.debug(`No room '${notificationRoomStringID}' found, creating new one`);
            yield db_1.default.query('INSERT INTO rooms (type, entity_id) VALUES (?, ?)', [NOTIFICATION_ROOM_TYPE, userID]);
            room = yield getRoom(NOTIFICATION_ROOM_TYPE, userID);
            yield db_1.default.query('INSERT INTO participants (user_id, room_id) VALUES (?, ?)', [userID, room.id]);
        }
        let metaJSON = JSON.stringify(meta), messageID = yield db_1.default.insertOne('INSERT INTO messages (room_id, type, message, meta_json) VALUES (?, ?, ?, ?)', [room.id, 'new_message', roomStringID, metaJSON]), message = yield db_1.default.queryOne('SELECT id, create_date FROM messages WHERE id = ?', [messageID]);
        db_1.default.query('DELETE FROM messages WHERE room_id = ? AND type = ? AND message = ? AND id != ?', [room.id, 'new_message', roomStringID, messageID]);
        yield db_1.default.query('UPDATE rooms SET is_empty = 0, last_action_date = ?, last_message_id = ? WHERE id = ?', [message.create_date, messageID, room.id]);
        messaging_1.default.sendNotification(userID, 'new_message', message.create_date, meta);
    });
}
function getRoom(type, entityID) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield db_1.default.queryOne('SELECT id FROM rooms WHERE type = ? AND entity_id = ?', [type, entityID]);
    });
}
exports.getRoom = getRoom;
function getRoomComplete(type, entityID) {
    return __awaiter(this, void 0, void 0, function* () {
        let room = yield db_1.default.queryOne('SELECT * FROM rooms WHERE type = ? AND entity_id = ?', [type, entityID]);
        if (room.meta_json) {
            room.meta = JSON.parse(room.meta_json);
            delete room.meta_json;
        }
        return room;
    });
}
exports.getRoomComplete = getRoomComplete;
