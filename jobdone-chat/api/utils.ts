import { info, error, debug } from 'winston';

import messaging from './messaging';
import db from '../db';


interface IMessage {
    type: string;
    text: string;
    meta?: any;
}

const NOTIFICATION_ROOM_TYPE = 'notification';

const FOLDER_LIST = [
    'inbox',
    'unread',
    'sent',
    'archive'
];


export async function checkRoomParticipant(roomStringID: string, userID: number) {
    let [ roomType, roomEntityID ] = roomStringID.split(':');
    if (!roomType || !roomEntityID) {
        throw new Error('Wrong room ID');
    }

    let allowed = !!(await db.queryOne('SELECT p.id FROM participants p, rooms r WHERE p.room_id = r.id AND r.type = ? AND r.entity_id = ? AND p.user_id = ?', [ roomType, roomEntityID, userID ]));

    if (!allowed && roomType === NOTIFICATION_ROOM_TYPE && !isNaN(+roomEntityID) && +roomEntityID === userID) {
        // For notification rooms we allow access based on userID and roomEntityID (which is userID for this type of room)
        // Room may not even exist (it's created once first notification is received from the backend)
        return true;
    }

    return allowed;
}


export async function markRoomAsRead(roomStringID: string, userID: number) {
    let [ roomType, roomEntityID ] = roomStringID.split(':');
    if (!roomType || !roomEntityID) {
        throw new Error('Wrong room ID');
    }

    await db.query(
        'UPDATE participants p, rooms r SET p.last_notified_message_id = (SELECT MAX(id) FROM messages WHERE room_id = r.id), p.last_read_message_id = p.last_notified_message_id WHERE p.room_id = r.id AND p.user_id = ? AND r.type = ? AND r.entity_id = ?',
        [ userID, roomType, roomEntityID ]
    );
}


export async function getMessages(roomStringID: string, params?: any, userID?: number) {
    let [ roomType, roomEntityID ] = roomStringID.split(':');
    if (!roomType || !roomEntityID) {
        throw new Error('Wrong room ID');
    }

    let offset = params && !isNaN(params.offset) ? params.offset : 0,
        limit = params && !isNaN(params.limit) ? params.limit : 4294967295;

    let messages = await db.query(
        'SELECT m.* FROM messages m, rooms r WHERE m.room_id = r.id AND r.type = ? AND r.entity_id = ? ORDER BY m.id DESC LIMIT ?, ?',
        [ roomType, roomEntityID, offset, limit ]
    );

    let total = (await db.queryOne(
        'SELECT count(*) as count FROM messages m, rooms r WHERE m.room_id = r.id AND r.type = ? AND r.entity_id = ?;',
        [ roomType, roomEntityID ]
    )).count;

    let unread = (await db.queryOne(
        'SELECT count(*) as count FROM messages m, rooms r, participants p WHERE m.room_id = r.id AND p.room_id = r.id AND r.type = ? AND r.entity_id = ? AND (p.last_read_message_id IS NULL OR p.last_read_message_id < m.id);',
        [ roomType, roomEntityID ]
    )).count;

    messages = messages.map(message => {
        return {
            id: message.id,
            date: message.create_date,
            meta: message.meta_json ? JSON.parse(message.meta_json) : null,
            type: message.type,
            user: message.user_id,
            text: message.message
        };
    });

    return {
        data: messages,
        meta: {
            total: total,
            unread: unread
        }
    };
}


export async function getRoomsWithCount(type: string, userID: number, params?: any) {
    let offset = params && !isNaN(params.offset) ? params.offset : 0,
        limit = params && !isNaN(params.limit) ? params.limit : 4294967295,
        folder = params && params.folder && FOLDER_LIST.indexOf(params.folder) !== -1 ? params.folder : null,
        rooms;

    let typeRequested = [type];
    if (type.indexOf(',') !== -1) {
        typeRequested = type.split(',');
    }
    
    let isUnreadSQL = (folder === 'unread' ? 'HAVING unread' : ''),
        isArchivedSQL = (folder === 'archive' ? 'AND p.is_archived' : 'AND NOT p.is_archived'),
        messageOriginSQL = '';

    if (folder === 'inbox' || folder === 'unread') {
        messageOriginSQL = 'AND r.last_message_id IS NOT NULL AND m.id = r.last_message_id AND (m.user_id != p.user_id OR EXISTS (SELECT COUNT(DISTINCT(m2.user_id)) as m2cnt FROM messages m2 WHERE m2.room_id = r.id HAVING m2cnt = 2))';
    } else if (folder === 'sent') {
        messageOriginSQL = 'AND r.last_message_id IS NOT NULL AND m.id = r.last_message_id AND m.user_id = p.user_id AND EXISTS (SELECT COUNT(DISTINCT(m2.user_id)) as m2cnt FROM messages m2 WHERE m2.room_id = r.id HAVING m2cnt = 1)';
    }

    rooms = await db.query(`
        SELECT
            r.*,
            (p.last_read_message_id IS NULL OR p.last_read_message_id < r.last_message_id) AS unread,
            CASE WHEN LENGTH(m.message) > 100 THEN CONCAT(SUBSTRING(m.message, 1, 100), '...') ELSE m.message END as message,
            p.is_archived
        FROM
            rooms r,
            participants p,
            messages m
        WHERE
            p.user_id = ?
            AND r.id = p.room_id
            AND r.type IN (?)
            ${messageOriginSQL}
            ${isArchivedSQL}
        GROUP BY r.id
        ${isUnreadSQL}
        ORDER BY
            r.last_action_date DESC,
            r.id DESC
        LIMIT ?, ?;`,
        [ userID, typeRequested, offset, limit ]
    );

    rooms = rooms.map(room => {
        if (room.meta_json) {
            room.meta = JSON.parse(room.meta_json);
            delete room.meta_json;
        }

        return room;
    });

    let total = (await db.queryOne(
        `SELECT count(*) as count FROM rooms r, participants p, messages m WHERE p.user_id = ? AND r.id = p.room_id AND r.type IN (?) ${messageOriginSQL} ${isArchivedSQL};`,
        [ userID, typeRequested ]
    )).count;

    let unread = (await db.queryOne(
        `SELECT COUNT(*) as count FROM rooms r, participants p, messages m WHERE p.user_id = ? AND r.id = p.room_id AND r.type IN (?) AND (p.last_read_message_id IS NULL OR p.last_read_message_id < r.last_message_id) ${messageOriginSQL} ${isArchivedSQL};`,
        [ userID, typeRequested ]
    )).count;

    return {
        data: rooms,
        meta: {
            total: total,
            unread: unread
        }
    };
}


export async function getRoomsCount(rooms: string[], userID: number) {
    let result = [];

    for (let room of rooms) {
        let total = (await db.queryOne(
            'SELECT count(*) as count FROM rooms r, participants p WHERE p.user_id = ? AND r.id = p.room_id AND r.type = ?;',
            [ userID, room ]
        )).count;

        let unread = (await db.queryOne(
            'SELECT COUNT(*) as count FROM rooms r, participants p WHERE p.user_id = ? AND r.id = p.room_id AND r.type = ? AND (p.last_read_message_id IS NULL OR p.last_read_message_id < r.last_message_id);',
            [ userID, room ]
        )).count;

        result.push({ total, unread });
    }

    return {
        data: result
    };
}


export async function archiveRooms(roomStringIDs: string[], userID: number) {
    let result = [];

    for (let roomStringID of roomStringIDs) {
        let [ roomType, roomEntityID ] = roomStringID.split(':');

        await db.query(`
            UPDATE rooms r, participants p
            SET p.is_archived = 1
            WHERE
                p.user_id = ?
                AND r.type = ?
                AND r.entity_id = ?
                AND p.room_id = r.id
        `, [ userID, roomType, roomEntityID ]);
    }

    return roomStringIDs;
}


export async function createMessage(roomStringID: string, senderID: number, message: IMessage) {
    let [ roomType, roomEntityID ] = roomStringID.split(':');
    if (!roomType || !roomEntityID) {
        throw new Error('Wrong room ID');
    }

    let room = await getRoom(roomType, +roomEntityID);
    if (!room) {
        throw new Error('Room not found');
    }

    let metaJSON = message.meta ? JSON.stringify(message.meta) : null,
        sender = senderID ? senderID : null,
        text = message.text ? message.text : null;
    
    let messageID = await db.insertOne('INSERT INTO messages (room_id, type, meta_json, message, user_id) VALUES (?, ?, ?, ?, ?)', [ room.id, message.type, metaJSON, text, sender ]),
        msg = await db.queryOne('SELECT id, create_date FROM messages WHERE id = ?', [ messageID ]);

    await db.query('UPDATE rooms SET is_empty = 0, last_action_date = ?, last_message_id = ? WHERE id = ?', [ msg.create_date, messageID, room.id ]);

    let participants = await db.query('SELECT user_id FROM participants WHERE room_id = ?', [ room.id ]);

    if (sender) {
        // Update last read message for sender
        await db.query('UPDATE participants SET last_read_message_id = ?, last_notified_message_id = ? WHERE room_id = ? AND user_id = ?', [ messageID, messageID, room.id, sender ]);

        if (message.meta && message.meta.notify) {
            // Create notification for other participants
            let otherParticipants = participants.filter(participant => participant.user_id !== sender);

            for (let participant of otherParticipants) {
                createNewMessageNotification(participant.user_id, roomStringID, { type: roomType, entityId: +roomEntityID, text: text });
            }
        }
    }

    for (let participant of participants) {
        messaging.sendMessage(participant.user_id, roomStringID, message.type, msg.create_date, message.meta, message.text, sender);
    }
}

async function createNewMessageNotification(userID: number, roomStringID: string, meta: any) {
    let room = await getRoom(NOTIFICATION_ROOM_TYPE, userID),
        notificationRoomStringID = [NOTIFICATION_ROOM_TYPE, userID].join(':');

    if (!room) {
        debug(`No room '${notificationRoomStringID}' found, creating new one`);

        await db.query('INSERT INTO rooms (type, entity_id) VALUES (?, ?)', [ NOTIFICATION_ROOM_TYPE, userID ]);

        room = await getRoom(NOTIFICATION_ROOM_TYPE, userID);

        // Add user to participants of his notification room
        await db.query('INSERT INTO participants (user_id, room_id) VALUES (?, ?)', [ userID, room.id ]);
    }

    let metaJSON = JSON.stringify(meta),
        messageID = await db.insertOne('INSERT INTO messages (room_id, type, message, meta_json) VALUES (?, ?, ?, ?)', [ room.id, 'new_message', roomStringID, metaJSON ]),
        message = await db.queryOne('SELECT id, create_date FROM messages WHERE id = ?', [ messageID ]);

    // Destroy old notification for the same room
    db.query('DELETE FROM messages WHERE room_id = ? AND type = ? AND message = ? AND id != ?', [ room.id, 'new_message', roomStringID, messageID ]);

    await db.query('UPDATE rooms SET is_empty = 0, last_action_date = ?, last_message_id = ? WHERE id = ?', [ message.create_date, messageID, room.id ]);

    messaging.sendNotification(userID, 'new_message', message.create_date, meta);
}


export async function getRoom(type: string, entityID: number) {
    return await db.queryOne('SELECT id FROM rooms WHERE type = ? AND entity_id = ?', [ type, entityID ]);
}


export async function getRoomComplete(type: string, entityID: number) {
    let room = await db.queryOne('SELECT * FROM rooms WHERE type = ? AND entity_id = ?', [ type, entityID ]);

    if (room.meta_json) {
        room.meta = JSON.parse(room.meta_json);
        delete room.meta_json;
    }

    return room;
}
