# SelfMarket Messaging Server

## Installation

Install [MariaDB](https://mariadb.org/download/).

Create `sm_messaging` database

```mysql
CREATE DATABASE sm_messaging;
```


Migrate

```js
npm run migrate
```


If you had `ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password:NO)` error, is possible to set `mariadb://root:PASSWORD@localhost/sm_messaging` in `.env` for a time.

