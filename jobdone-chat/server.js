const express = require('express');
const winston = require('winston');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const cors = require('cors');
const sockjs = require('sockjs');


// Initializing .env file
dotenv.config({ silent: true });


// Configuring logger
winston.configure({
    level: process.env.LOG_LEVEL || 'info',
    transports: [
        new (winston.transports.Console)({ colorize: true })
    ]
});

winston.setLevels(winston.config.npm.levels);
winston.addColors(winston.config.npm.colors);


const app = express();

app.set('secret', process.env.SECRET);
app.set('host', process.env.HOST || '127.0.0.1');
app.set('port', process.env.PORT || 5001);
app.set('base_uri', process.env.BASE_URI || 'http://127.0.0.1:5001');


// Check that SECRET is specified
if (!app.get('secret')) {
    winston.error('SECRET environment variable is required. Generate strong secret key as it is used to sign requests');
    process.exit(1);
}

if (process.env.NODE_ENV !== 'production') {
    winston.info('Server running in development mode');
} else {
    winston.info('Server running in production mode');
}

// Use body parser for every request
app.use(bodyParser.json());


// For non-production environments add CORS headers
if (process.env.NODE_ENV !== 'production') {
    app.use(cors());
}


// Mount routes
app.use('/api/private', require('./api/private'));


// Initialize database and messaging and run the server
const database = require('./db').default;
const messaging = require('./api/messaging').default;

database.connect(process.env.DB_CONNECTION_STRING).then(() => {
    let server = app.listen(app.get('port'), app.get('host'), () => winston.info(`Server has been started on ${app.get('host')}:${app.get('port')}`)),
        socketServer = sockjs.createServer({ sockjs_url: '//cdn.jsdelivr.net/sockjs/1.1.2/sockjs.min.js' });

    socketServer.installHandlers(server, { prefix: '/ws' });
    messaging.initialize(socketServer);
}).catch(err => {
    winston.error('Unable to create connection to the database:', err);
    process.exit(1);
});


// Detect unhandled promise rejections
process.on('unhandledRejection', (reason) => {
    winston.error('UnhandledRejection:', reason.stack ? reason.stack : reason);
});


module.exports.app = app;
