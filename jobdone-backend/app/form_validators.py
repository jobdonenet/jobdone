__author__ = 'Constantine Slednev <c.slednev@gmail.com>'


from wtforms import ValidationError
from app.models import Skills


class S3FilesValidator(object):
    def __init__(self, fields=None):
        if fields:
            self.fields = set(fields)
        else:
            self.fields = set(('filename', 'attachmentId', 'size',))

    def __call__(self, form, field):
        if not field.data:
            return

        if type(field.data) is not list:
            raise ValidationError('Wrong field format')

        for item in field.data:
            if set(item) != self.fields:
                raise ValidationError('Wrong attachments format')


class SkillsValidator(object):
    def __init__(self, min_skills=0, max_skills=10):
        self.min_skills = min_skills
        self.max_skills = max_skills

    def __call__(self, form, field):
        if not field.data:
            return

        if type(field.data) is not list:
            raise ValidationError('Wrong field format')

        if len(field.data) < self.min_skills:
            raise ValidationError('Minimum %d skill required' % self.min_skills)

        if len(field.data) > self.max_skills:
            raise ValidationError('Maximum %d skill possible' % self.max_skills)

        skills_in_database = Skills.query.filter(Skills.id.in_(field.data)).count()
        print skills_in_database
        if skills_in_database != len(field.data):
            raise ValidationError('Skills unknown')

