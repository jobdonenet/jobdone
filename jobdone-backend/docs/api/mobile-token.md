# To get token

Request:

```bash
curl -v -d '{"username":"admin","password":"admin"}' 'http://localhost:5000/api/auth/jwt/login' -H "X-Requested-With: xmlhttprequest" -H "Content-Type: application/json" 
```

Response:

```
{"access_token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZGVudGl0eSI6MSwiaWF0IjoxNTE3NTE1OTA1LCJuYmYiOjE1MTc1MTU5MDUsImV4cCI6MTUxNzUxNjIwNX0.1yQBFpH9sldH59jRO_jtpZlquqYNc-plsiBm_ib1a0k"}
```


# Try to get data from private area:


```
curl -v 'http://localhost:5000/dashboard' -H "X-Requested-With: xmlhttprequest" -H "Authorization: JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZGVudGl0eSI6MSwiaWF0IjoxNTE3NTE1OTA1LCJuYmYiOjE1MTc1MTU5MDUsImV4cCI6MTUxNzUxNjIwNX0.1yQBFpH9sldH59jRO_jtpZlquqYNc-plsiBm_ib1a0k"
```


# To refresh token

request :

```bash
curl -v 'http://localhost:5000/api/auth/jwt/refresh' -H "X-Requested-With: xmlhttprequest" -H "Authorization: JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZGVudGl0eSI6MSwiaWF0IjoxNTE3NTE5ODU0LCJuYmYiOjE1MTc1MTk4NTQsImV4cCI6MTUxNzUyMDE1NH0.xMq2U8Pdkc1GvHImqvbdniFLBsROIlh9URFoRbVHAA0"
```

Response:

```
{"access_token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZGVudGl0eSI6MSwiaWF0IjoxNTE3NTE5OTY5LCJuYmYiOjE1MTc1MTk5NjksImV4cCI6MTUxNzUyMDI2OX0.oo1oXJT7MBFjnwng2ge6Y29eMzLCyoLyGMmFpJPw3Mw"}
```



# Config

To change token expiration delta use config argument

```
JWT_EXPIRATION_DELTA = timedelta(seconds=300)
```